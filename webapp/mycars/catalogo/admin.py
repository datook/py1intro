from django.contrib import admin
from .models import Costruttore, Auto 

# Register your models here.
admin.site.register(Costruttore, admin.ModelAdmin)
admin.site.register(Auto, admin.ModelAdmin)
