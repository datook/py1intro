from django.db import models

# Create your models here.
class Costruttore(models.Model):
    class Meta:
        verbose_name_plural = "Costruttori"

    nome = models.CharField(max_length=50, unique=True)
    
    def __str__(self):
        return self.nome

TIPI_ALIMENTAZIONE = (
    ('b', "Benzina"),
    ('d', "Diesel"),
    ('g', "GPL"),
    ('m', "Metano"),
    ('e', "Elettrica"),
    ('i', "Ibrida"),
)

class Auto(models.Model):
    
    class Meta:
        verbose_name_plural = "Auto"

    costruttore = models.ForeignKey(Costruttore, on_delete=models.PROTECT)
    modello = models.CharField(max_length=50)
    anno = models.IntegerField()
    
    alimentazione = models.CharField(max_length=3, choices=TIPI_ALIMENTAZIONE)
    km = models.IntegerField(blank=True, default=0)

    fotografia = models.ImageField(blank=True)
    note = models.TextField(blank=True)

    def __str__(self):
        return "{} {} anno {}".format(self.costruttore, self.modello, self.anno)
     