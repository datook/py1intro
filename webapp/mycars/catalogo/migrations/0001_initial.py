# Generated by Django 2.1.7 on 2019-03-16 10:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Auto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('modello', models.CharField(max_length=50)),
                ('anno', models.IntegerField()),
                ('alimentazione', models.CharField(choices=[(0, 'Benzina'), (1, 'Diesel'), (2, 'GPL'), (3, 'Metano'), (4, 'Elettrica'), (5, 'Ibrida')], max_length=50)),
                ('km', models.IntegerField(blank=True, default=0)),
                ('fotografia', models.ImageField(blank=True, upload_to='')),
                ('note', models.TextField(blank=True)),
            ],
            options={
                'verbose_name_plural': 'Auto',
            },
        ),
        migrations.CreateModel(
            name='Costruttore',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=50, unique=True)),
            ],
            options={
                'verbose_name_plural': 'Costruttori',
            },
        ),
        migrations.AddField(
            model_name='auto',
            name='costruttore',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='catalogo.Costruttore'),
        ),
    ]
