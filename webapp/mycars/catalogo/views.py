import django_filters
from django.shortcuts import render

from catalogo.models import Auto


class AutoFilter(django_filters.FilterSet):
    class Meta:
        model = Auto
        fields = ['costruttore', 'modello', 'anno']


def ricerca_auto(request):
    filtro = AutoFilter(request.GET, queryset=Auto.objects.all())
    return render(request, 'ricerca_auto.html', {'filter': filtro})
