"""
Chiede il nome di una città e ne mostra la situazione meteo
utilizzando l'API di OpenWeatherMap.org
"""
# Librerie utilizzate
import requests


# Un po' di costanti
API_KEY = "2c353b8c110db8fe8eca5381b4c9635b"  # La mia chiave di sviluppatore
API_ADDRESS = "http://api.openweathermap.org/data/2.5/weather?appid={}&lang=it&q=".format(API_KEY)


# Definisco la funzione per il reperimento delle informazioni meteo
def dammi_meteo(citta):
    """
    Stampa il meteo di una determinata città
    """
    # Compone l'url aggiungendo il nome della città all'indirizzo base
    url = API_ADDRESS + citta

    # Effettua la chiamata e restituisce i dati in formato JSON
    risposta = requests.get(url)
    dati_json = risposta.json()

    # Controlla l'esito della richiesta
    if risposta.status_code != 200:
        messaggio = dati_json['message']
        return messaggio
    
    # Recupera dalla risposta la descrizione della situazione meteo
    descrizione = dati_json['weather'][0]['description'].capitalize()

    # Restituisce tale descrizione
    return descrizione


# Chiede la città (Invio per terminare)
citta = input("Città (Invio per uscire): ")
while citta:
    # Richiede la situazione meteo e la stampa
    situazione = dammi_meteo(citta)
    print(situazione)
    
    # Chiede la città e ricomincia il ciclo
    citta = input("Città (Invio per uscire): ")
